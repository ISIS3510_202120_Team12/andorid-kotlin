package com.example.corazonsaludable.model

data class DataPregunta(
    var especialidad: String = "",
    var pregunta: String = "",
    var tema: String = ""
)