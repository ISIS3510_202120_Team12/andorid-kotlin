package com.example.corazonsaludable.model

import kotlin.collections.ArrayList

data class PreferencesModel(
    var alergias: ArrayList<Boolean>,
    var casa: ArrayList<Double>,
    var celiaco: Boolean,
    var deportes: ArrayList<String>,
    var vegano: Boolean,
    var vegetariano: Boolean
)
