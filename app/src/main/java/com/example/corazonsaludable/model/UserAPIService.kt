package com.example.corazonsaludable.model

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


interface UserAPIService {


    @GET
    suspend fun getUsers(): Call<List<UserModel?>?>?
    @GET("{id}")
    suspend fun getUser(): Call<List<UserModel?>?>?

    @POST
    suspend fun addUser(@Body user: UserModel?): Call<UserModel?>?

    @PUT("{id}")
    suspend fun updateUser(@Path("id") id: String, @Body user: UserModel?)

    @DELETE("{id}")
    suspend fun deleteUser(@Path("id") id: Int): Call<UserModel?>?
}