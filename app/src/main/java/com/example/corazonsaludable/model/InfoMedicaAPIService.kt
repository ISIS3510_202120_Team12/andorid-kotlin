package com.example.corazonsaludable.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.http.*

interface InfoMedicaAPIService {

    @GET
    suspend fun getUsers(): Call<List<InfoMedicalModel?>?>?
    @GET("{id}")
    suspend fun getUser(): Call<List<InfoMedicalModel?>?>?

    @POST
    suspend fun addUser(@Body user: InfoMedicalModel?): Call<InfoMedicalModel?>?

    @PUT("{id}")
    suspend fun updateUser(@Path("id") id: String, @Body user: InfoMedicalModel?)

    @DELETE("{id}")
    suspend fun deleteUser(@Path("id") id: Int): Call<InfoMedicalModel?>?
}