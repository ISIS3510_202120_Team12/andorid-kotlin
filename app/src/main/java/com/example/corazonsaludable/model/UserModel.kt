package com.example.corazonsaludable.model

import java.time.LocalDate
import java.util.*


data class UserModel(
    var nombreCompleto: String,
    var deptoCardio: String,
    var empleado: Boolean,
    var fechaNacimiento:String,
    var sexo: String,
    var rol: Int
)
