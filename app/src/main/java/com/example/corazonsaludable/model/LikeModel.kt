package com.example.corazonsaludable.model

import java.util.*


data class LikeModel(
    var idTip: String,
    var idUsuario: String,
    var meGusto: Boolean
)
