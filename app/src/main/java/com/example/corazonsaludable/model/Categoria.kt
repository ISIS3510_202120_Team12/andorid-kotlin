package com.example.corazonsaludable.model

data class Categoria(
    var name: String = "",
    var msg: String = "",
    var selected: Boolean = false

)