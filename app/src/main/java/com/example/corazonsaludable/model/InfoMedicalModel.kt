package com.example.corazonsaludable.model

import java.util.*
import kotlin.collections.ArrayList


data class InfoMedicalModel(
    var altura: Int,
    var colesterolTotal: Int,
    var consumoAlcohol: Boolean,
    var diabetico: Boolean,
    var fumador: Boolean,
    var peso: Int,
    var presionSistolica: Int,
    var spo2: Int
)
