package com.example.corazonsaludable.model

data class DataRestaurante(
    var name: String = "",
    var direccion: String = "",
    var precio: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
)