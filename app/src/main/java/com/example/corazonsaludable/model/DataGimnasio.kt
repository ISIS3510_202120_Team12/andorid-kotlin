package com.example.corazonsaludable.model

data class DataGimnasio(
    var name: String = "",
    var direccion: String = "",
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
)