package com.example.corazonsaludable.model

data class CategoriaItem(
    var name: String = "",
    var msg: String = "",
    var selected: Boolean = false
)
