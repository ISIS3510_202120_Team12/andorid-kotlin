package com.example.corazonsaludable.view

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.corazonsaludable.tools.Utils
import androidx.appcompat.app.AppCompatActivity
import com.example.corazonsaludable.R
import com.example.corazonsaludable.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val intentIniciarSesion = Intent(this,LoginActivity::class.java)
        val intentRegistrarse = Intent(this,RegisterActivity::class.java)
        binding.buttonIniciarSesion.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                startActivity(intentIniciarSesion);
            }
        })
        binding.buttonCrearCuenta.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                startActivity(intentRegistrarse)
            }
        })
        binding.imageButton2.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                if(Utils().isConnected(this@MainActivity)){
                    val browserIntent =
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://cardioinfantil.org/"))
                        startActivity(browserIntent)
                }
                else{
                    val builder = AlertDialog.Builder(this@MainActivity)
                    val msg: String = "Oops! Parece que no estás conectado a internet. Intentalo más tarde."
                    val yes: String = "Entendido"
                    builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(yes) { dialog, _ ->
                            dialog.dismiss()
                        }
                    val alert = builder.create()
                    alert.show()
                }

            }
        })

    }
    }