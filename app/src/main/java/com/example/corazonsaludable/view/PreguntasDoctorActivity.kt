package  com.example.corazonsaludable.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModelProvider
import com.example.corazonsaludable.R
import com.example.corazonsaludable.viewmodel.PreguntasViewModel
import com.example.corazonsaludable.viewmodel.RecomendacionesViewModel

class PreguntasDoctorActivity : AppCompatActivity() {

    // View Models
    private lateinit var preguntasViewModel: PreguntasViewModel


    private lateinit var spinnerEspecialidades: Spinner
    private lateinit var spinnerTemas: Spinner
    private lateinit var btnPreguntar: Button
    private lateinit var txtPregunta: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preguntas_doctor)

        preguntasViewModel =
            ViewModelProvider(this@PreguntasDoctorActivity)[PreguntasViewModel::class.java]

        spinnerEspecialidades = findViewById(R.id.especialidades_spinner)
        spinnerTemas = findViewById(R.id.temas_spinner)
        btnPreguntar = findViewById(R.id.button_preguntar)
        txtPregunta = findViewById(R.id.input_pregunta)

        // SPINNER ESPECIALIDADES

        spinnerEspecialidades.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
        ArrayAdapter.createFromResource(
            this,
            R.array.especialidades_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerEspecialidades.adapter = adapter
        }

        // SPINNER TEMAS

        spinnerTemas.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

        }
        ArrayAdapter.createFromResource(
            this,
            R.array.temas_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinnerTemas.adapter = adapter
        }

        btnPreguntar.setOnClickListener {
            if (txtPregunta.text.toString() != "") {

            } else {
                Toast.makeText(
                    this,
                    "No puedes enviar una pregunta en blanco",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}