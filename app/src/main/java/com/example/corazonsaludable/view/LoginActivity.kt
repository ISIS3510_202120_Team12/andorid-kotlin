package com.example.corazonsaludable.view

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.corazonsaludable.R
import com.example.corazonsaludable.databinding.ActiviyLoginBinding
import com.example.corazonsaludable.tools.Constants
import com.example.corazonsaludable.tools.ManagerPreferences
import com.example.corazonsaludable.tools.Utils
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity(){
    private lateinit var binding: ActiviyLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActiviyLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val intentRegistrate = Intent(this,RegisterActivity::class.java)
        binding.textViewRegistrate.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {
                    startActivity(intentRegistrate)
            }
        })
        binding.buttonInicioSesion.setOnClickListener{
            val email: String = binding.editTextEmailSignIn.text.toString().trim { it <= ' '};
            val password:String = binding.editTextPasswordSignIn.text.toString().trim { it <= ' '}
            if(Utils().isConnected(this@LoginActivity)){
                when {
                    TextUtils.isEmpty(email) -> {
                        Toast.makeText(
                            this@LoginActivity,
                            "Por favor ingrese un correo electrónico",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    TextUtils.isEmpty(password) -> {
                        Toast.makeText(
                            this@LoginActivity,
                            "Por favor ingrese una contraseña",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    else -> {
                        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    Toast.makeText(
                                        this@LoginActivity,
                                        "Inicio de sesión exitoso!",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    // Se guardan en las preferencias la llave generada por FirebaseAuth para consultar información
                                    // del usuario posteriormente.
                                    ManagerPreferences().putString(
                                        this,
                                        Constants.USER_ID_FIREBASE,
                                        task.result.user!!.uid
                                    )
                                    // task.result.user!!.uid
                                    val intent =
                                        Intent(
                                            this@LoginActivity,
                                            BottomNavigationActivity::class.java
                                        )
                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(
                                        this@LoginActivity,
                                        task.exception!!.message.toString(),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                    }
                }
            }
            else{
                val builder = AlertDialog.Builder(this@LoginActivity)
                val msg: String = "Oops! Parece que no estás conectado a internet. Intentalo más tarde."
                val yes: String = "Entendido"
                builder.setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(yes) { dialog, _ ->
                        dialog.dismiss()
                    }
                val alert = builder.create()
                alert.show()
            }

            }
        }
    }
