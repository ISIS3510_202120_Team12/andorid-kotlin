package com.example.corazonsaludable.view.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.corazonsaludable.R
import com.example.corazonsaludable.model.DataGimnasio
import com.example.corazonsaludable.tools.Utils
import com.example.corazonsaludable.view.adapters.AdapterGimnasiosListDialog
import kotlinx.android.synthetic.main.dialog_list_recomendaciones.view.*

class GimnasiosDialogCustom(private var list: ArrayList<DataGimnasio>) : DialogFragment() {

    // Categorias list recycler view
    private lateinit var recomendacionesListRecycler: RecyclerView
    private var adapterGimnasios: AdapterGimnasiosListDialog? = null

    private lateinit var closeDialog: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView: View =
            inflater.inflate(R.layout.dialog_list_recomendaciones, container, false)
        recomendacionesListRecycler = rootView.lista_recomendaciones
        recomendacionesListRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recomendacionesListRecycler.setHasFixedSize(true)

        closeDialog = rootView.btn_close_dialog

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeDialog.setOnClickListener {
            dismiss()
        }
        updateItemsCategoria()
    }

    private fun updateItemsCategoria() {
        adapterGimnasios = AdapterGimnasiosListDialog(
            requireContext(),
            list,
            object : AdapterGimnasiosListDialog.OnSelectCategorialistener {
                override fun seleccionarGimnasio(item: DataGimnasio) {
                    Utils().launchWaze(requireContext(),item.latitude,item.longitude)
                }
            })
        recomendacionesListRecycler.adapter = adapterGimnasios
        adapterGimnasios?.notifyDataSetChanged()
    }
}