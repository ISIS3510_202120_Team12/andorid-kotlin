package com.example.corazonsaludable.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.corazonsaludable.R
import com.example.corazonsaludable.model.CategoriaItem
import kotlinx.android.synthetic.main.item_list_category_item.view.*

class AdapterItemCategoria(
    private val context: Context,
    private val values: ArrayList<CategoriaItem>,
    private val listener: OnSelectCategoriaItemlistener
) : RecyclerView.Adapter<AdapterItemCategoria.ViewHolder>() {

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        var mClickCategory: LinearLayout = mView.card_item_category_item
        var mImgCategory: ImageView = mView.image_category_item
        var mTitleCategory: TextView = mView.title_category_item
        var mMsgCategory: TextView = mView.msg_category_item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_category_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.mTitleCategory.text = item.name
        holder.mMsgCategory.text = item.msg
        holder.mClickCategory.setOnClickListener {
            listener.seleccionaCategoriaItem(item)
        }
    }

    interface OnSelectCategoriaItemlistener {
        fun seleccionaCategoriaItem(item: CategoriaItem)
    }
}