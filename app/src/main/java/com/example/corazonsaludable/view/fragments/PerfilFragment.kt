package com.example.corazonsaludable.view.fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.example.corazonsaludable.R
import com.example.corazonsaludable.model.UserModel
import com.example.corazonsaludable.tools.ManagerPreferences
import com.example.corazonsaludable.viewmodel.UserViewModel

/**
 * A simple [Fragment] subclass.
 */
class PerfilFragment : Fragment() {

    // View Models
    private lateinit var userViewModel: UserViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        userViewModel =
            ViewModelProvider(this@PerfilFragment)[UserViewModel::class.java]
        userViewModel.getUserInfo(requireContext(), object : UserViewModel.OnResponseDataUser {
            override fun onResponseSuccessFullDataUser(user: UserModel) {
                Log.e("USER", user.nombreCompleto)
            }
        })
        return inflater.inflate(R.layout.fragment_perfil, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.nombre_user).text=
            context?.let { ManagerPreferences().getString(it,"nom","") }
        view.findViewById<TextView>(R.id.departamento_user).text=
            context?.let { ManagerPreferences().getString(it,"dept","") }
        view.findViewById<TextView>(R.id.fecha_nacimiento_user).text=
            context?.let { ManagerPreferences().getString(it,"fechNac","") }
        view.findViewById<TextView>(R.id.sexo_user).text=
            context?.let { ManagerPreferences().getString(it,"sex","") }
        var emple="No"
        if(context?.let { ManagerPreferences().getBoolean(it,"emp",true) } == true){
            emple="Si"
        }
        view.findViewById<TextView>(R.id.es_empleado_user).text=emple
    }
}