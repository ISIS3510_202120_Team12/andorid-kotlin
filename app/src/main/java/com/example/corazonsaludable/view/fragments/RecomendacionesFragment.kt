package com.example.corazonsaludable.view.fragments

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.corazonsaludable.R
import com.example.corazonsaludable.model.Categoria
import com.example.corazonsaludable.model.CategoriaItem
import com.example.corazonsaludable.model.DataGimnasio
import com.example.corazonsaludable.model.DataRestaurante
import com.example.corazonsaludable.tools.Constants
import com.example.corazonsaludable.tools.Utils
import com.example.corazonsaludable.view.adapters.AdapterCategorias
import com.example.corazonsaludable.view.adapters.AdapterItemCategoria
import com.example.corazonsaludable.view.dialogs.GimnasiosDialogCustom
import com.example.corazonsaludable.view.dialogs.RestaurantesDialogCustom
import com.example.corazonsaludable.viewmodel.RecomendacionesViewModel

/**
 * A simple [Fragment] subclass.
 */
class RecomendacionesFragment : Fragment() {


    // View Models
    private lateinit var recomendacionesViewModel: RecomendacionesViewModel

    //Titulo de categoria seleccionada
    private lateinit var titleCategoriaSeleccionada: TextView

    // Categorias list recycler view
    private lateinit var categoriasListRecycler: RecyclerView
    private var adapterCategorias: AdapterCategorias? = null

    // Lista de categorias
    private val listaCategorias = ArrayList<Categoria>()

    // Categorias list recycler view
    private lateinit var itemsCategoriaRecycler: RecyclerView
    private var adapterItemCategoria: AdapterItemCategoria? = null

    // Categorias
    // Lista de categorias
    private val cat1 = Categoria("Nutrición", "No consumir azúcar", true)
    private val cat2 = Categoria("Ejercicio", "Correr 20 min", false)
    private val cat3 = Categoria("Meditación", "Medita 30 min", false)
    private val cat4 = Categoria("Sueño", "Duerme 8 horas", false)

    // Items Categorias
    // Lista de categorias
    private val subcat1 = CategoriaItem("Restaurantes", "Come saludable", false)
    private val subcat2 = CategoriaItem("Gimnasios", "Disfruta del ejercicio", false)

    // Lista de items de una categoria
    private val listaItemsCategoria = ArrayList<CategoriaItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_recomendaciones, container, false)

        recomendacionesViewModel = ViewModelProvider(this@RecomendacionesFragment)[RecomendacionesViewModel::class.java]

        categoriasListRecycler = view.findViewById(R.id.lista_categorias)
        categoriasListRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        categoriasListRecycler.setHasFixedSize(true)

        itemsCategoriaRecycler = view.findViewById(R.id.items_categoria)
        itemsCategoriaRecycler.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        itemsCategoriaRecycler.setHasFixedSize(true)

        titleCategoriaSeleccionada = view.findViewById(R.id.title_category_selected)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateRecyclerCategorias()
    }

    private fun updateClickCategoria(item: Categoria) {
        val nuevoTitulo: String = "Recomendaciones de " + "${item.name}"
        titleCategoriaSeleccionada.text = nuevoTitulo
        listaItemsCategoria.clear()
        if (item.name == "Nutrición") {
            cat1.selected = true
            cat2.selected = false
            listaItemsCategoria.add(subcat1)
        } else if (item.name == "Ejercicio") {
            cat1.selected = false
            cat2.selected = true
            listaItemsCategoria.add(subcat2)
        }
        updateItemsCategoria()
    }

    private fun updateItemsCategoria() {
        adapterItemCategoria = AdapterItemCategoria(
            requireContext(),
            listaItemsCategoria,
            object : AdapterItemCategoria.OnSelectCategoriaItemlistener {
                override fun seleccionaCategoriaItem(item: CategoriaItem) {
                    when (item.name) {
                        Constants.ITEM_CATEGORIA_RESTAURANTE -> {
                            if (Utils().isConnected(context!!)) {
                                recomendacionesViewModel.getRestaurantes(object : RecomendacionesViewModel.OnResponseRestaurantes{
                                    override fun onResponseSuccessFullRestaurantes(restaurantes: ArrayList<DataRestaurante>) {
                                        // se obtienen los restaurantes
                                        val dialogRestaurante = RestaurantesDialogCustom(restaurantes)
                                        dialogRestaurante.show(childFragmentManager,Constants.CUSTOM_RESTAURANTES)
                                    }
                                })
                            } else {
                                val builder = AlertDialog.Builder(context)
                                val msg: String = "Oops! Parece que no estás conectado a internet. Intentalo más tarde."
                                val yes: String = "Entendido"
                                builder.setMessage(msg)
                                    .setCancelable(false)
                                    .setPositiveButton(yes) { dialog, _ ->
                                        dialog.dismiss()
                                    }
                                val alert = builder.create()
                                alert.show()
                            }
                        }
                        Constants.ITEM_CATEGORIA_GIMNASIO -> {
                            if (Utils().isConnected(context!!)) {
                                recomendacionesViewModel.getGimnasios(object : RecomendacionesViewModel.OnResponseGimnasios {
                                    override fun onResponseSuccessFullGimnasios(gimnasios: ArrayList<DataGimnasio>) {
                                        // se obtienen los restaurantes
                                        val dialogRestaurante = GimnasiosDialogCustom(gimnasios)
                                        dialogRestaurante.show(childFragmentManager,Constants.CUSTOM_RESTAURANTES)                                }
                                })
                            } else {
                                val builder = AlertDialog.Builder(context)
                                val msg: String = "Oops! Parece que no estás conectado a internet. Intentalo más tarde."
                                val yes: String = "Entendido"
                                builder.setMessage(msg)
                                    .setCancelable(false)
                                    .setPositiveButton(yes) { dialog, _ ->
                                        dialog.dismiss()
                                    }
                                val alert = builder.create()
                                alert.show()
                            }
                        }
                    }

                }
            })
        itemsCategoriaRecycler.adapter = adapterItemCategoria
        adapterItemCategoria?.notifyDataSetChanged()
    }

    /**
     * Método que actualiza el adapter de las categorias.
     */
    private fun updateRecyclerCategorias() {
        adapterCategorias = AdapterCategorias(
            requireContext(),
            listaCategorias,
            object : AdapterCategorias.OnSelectCategorialistener {
                override fun seleccionaCategoria(item: Categoria) {
                    updateClickCategoria(item)
                }
            })
        categoriasListRecycler.adapter = adapterCategorias
        adapterCategorias?.notifyDataSetChanged()
    }

    override fun onPause() {
        listaCategorias.clear()
        listaItemsCategoria.clear()
        adapterCategorias?.notifyDataSetChanged()
        adapterItemCategoria?.notifyDataSetChanged()
        super.onPause()
    }

    override fun onStop() {
        listaCategorias.clear()
        listaItemsCategoria.clear()
        adapterCategorias?.notifyDataSetChanged()
        adapterItemCategoria?.notifyDataSetChanged()
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        // Se agregan las categorias
        listaCategorias.add(cat1)
        listaCategorias.add(cat2)
        listaCategorias.add(cat3)
        listaCategorias.add(cat4)
        // Se agregan items de cada categoria
        if (cat1.selected) {
            listaItemsCategoria.add(subcat1)
        } else if (cat2.selected) {
            listaItemsCategoria.add(subcat2)
        }
        updateItemsCategoria()
    }
}