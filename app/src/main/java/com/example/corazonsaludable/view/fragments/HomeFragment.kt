package com.example.corazonsaludable.view.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import com.example.corazonsaludable.R
import com.example.corazonsaludable.view.BPM
import com.example.corazonsaludable.view.Calculadora
import com.example.corazonsaludable.view.PreguntasDoctorActivity


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<CardView>(R.id.cardLatidosPorMinuto).setOnClickListener(View.OnClickListener {
            val intent = Intent(context,BPM::class.java)
            startActivity(intent)
        })
        view.findViewById<CardView>(R.id.cardDoctor).setOnClickListener(View.OnClickListener {
            val intent = Intent(context,PreguntasDoctorActivity::class.java)
            startActivity(intent)
        })
        view.findViewById<CardView>(R.id.cardViewCalculadora).setOnClickListener(View.OnClickListener {
            val intent = Intent(context,Calculadora::class.java)
            startActivity(intent)
        })
    }
}