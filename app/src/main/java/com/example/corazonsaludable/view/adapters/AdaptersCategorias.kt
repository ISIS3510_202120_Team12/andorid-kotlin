package com.example.corazonsaludable.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.corazonsaludable.R
import com.example.corazonsaludable.model.Categoria
import com.example.corazonsaludable.tools.Constants
import kotlinx.android.synthetic.main.item_list_category.view.*

class AdapterCategorias(
    private val context: Context,
    private val values: ArrayList<Categoria>,
    private val listener: OnSelectCategorialistener
) : RecyclerView.Adapter<AdapterCategorias.ViewHolder>() {

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        var mClickCategory: LinearLayout = mView.card_item_category
        var mImgCategory: ImageView = mView.img_category
        var mTitleCategory: TextView = mView.title_category
        var mMsgCategory: TextView = mView.msg_category
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_category, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.mTitleCategory.text = item.name
        holder.mMsgCategory.text = item.msg
        holder.mClickCategory.setOnClickListener {
            listener.seleccionaCategoria(item)
        }

        if (item.name != Constants.CATEGORIA_NUTRIC && item.name != Constants.CATEGORIA_EJERCIC) {
            holder.mTitleCategory.setTextColor(context.getColor(R.color.light_steel_blue))
            holder.mMsgCategory.setTextColor(context.getColor(R.color.light_steel_blue))
            holder.mImgCategory.setImageResource(R.drawable.ic_category_unavailable)
        }
    }

    interface OnSelectCategorialistener {
        fun seleccionaCategoria(item: Categoria)
    }
}