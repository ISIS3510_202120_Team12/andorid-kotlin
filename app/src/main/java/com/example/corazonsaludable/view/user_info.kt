package com.example.corazonsaludable.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.corazonsaludable.R
import com.example.corazonsaludable.databinding.ActivityUserInfoBinding
import com.example.corazonsaludable.model.UserAPIService
import com.example.corazonsaludable.model.UserModel
import com.google.firebase.auth.FirebaseAuth

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import android.widget.AdapterView
import com.example.corazonsaludable.tools.ManagerPreferences
import com.example.corazonsaludable.tools.Utils
import kotlinx.android.synthetic.main.activity_user_info.*
import java.time.LocalDate.*


class user_info : AppCompatActivity() {
    var sexo=""
    var deptoCardio=""
    private lateinit var binding: ActivityUserInfoBinding
    var currentFirebaseUser = FirebaseAuth.getInstance().currentUser
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityUserInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intentHome = Intent(this,BottomNavigationActivity::class.java)
        radioGroup.setOnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            when(checkedId){
                radioButton.id -> sexo="M"
                else -> {
                    sexo="F"
                }
            }
        }
        binding.spinnerDept?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (parent != null) {
                    deptoCardio=parent.getItemAtPosition(position).toString()
                }

            }

        }
        binding.buttonGuardar.setOnClickListener(object : View.OnClickListener{
            override fun onClick(view: View?) {

                val empleado=binding.switchEmpleado.isChecked
                val fechaNacimiento= binding.editTextDate.text.toString()
                val nombreCompleto=binding.editTextTextPersonName.text.toString();
                val rol=1
                val user = UserModel(nombreCompleto,deptoCardio,empleado,fechaNacimiento,sexo,rol)
                saveUserInfo(user)
                ManagerPreferences().putString(this@user_info,"nom",nombreCompleto)
                ManagerPreferences().putBoolean(this@user_info,"emp",empleado)
                ManagerPreferences().putString(this@user_info,"dept",deptoCardio)
                ManagerPreferences().putString(this@user_info,"fechNac",fechaNacimiento)
                ManagerPreferences().putString(this@user_info,"sex",sexo)
                ManagerPreferences().putBoolean(this@user_info,"UIUpdate",true)
                startActivity(intentHome)
            }
        })
        val spinDept=binding.spinnerDept
        ArrayAdapter.createFromResource(
            this,
            R.array.departments_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinDept.adapter=adapter
        }
        binding.switchEmpleado.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked){
                binding.departamento.visibility=View.VISIBLE
                spinDept.visibility=View.VISIBLE
            }
        }


    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun saveUserInfo(user: UserModel){
        CoroutineScope(Dispatchers.IO).launch {
            val call = currentFirebaseUser?.let {
                while(!Utils().isConnected(this@user_info)){
                    continue
                }
                getRetrofit().create(UserAPIService::class.java).updateUser(
                    it.uid,user)
            }
        }
    }
}