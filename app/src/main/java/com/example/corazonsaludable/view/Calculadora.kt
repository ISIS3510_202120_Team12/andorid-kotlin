package com.example.corazonsaludable.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.corazonsaludable.R
import com.example.corazonsaludable.databinding.ActivityCalculadoraBinding
import com.example.corazonsaludable.model.InfoMedicaAPIService
import com.example.corazonsaludable.model.InfoMedicalModel
import com.example.corazonsaludable.model.UserAPIService
import com.example.corazonsaludable.model.UserModel
import com.example.corazonsaludable.tools.Utils
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_calculadora.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Calculadora : AppCompatActivity() {
    var currentFirebaseUser = FirebaseAuth.getInstance().currentUser
    private lateinit var binding: ActivityCalculadoraBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalculadoraBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.button.setOnClickListener{
            val consumoAlcohol=switchAlcohol.isChecked
            val diabetico=switchAzucar.isChecked
            val fumador=switchCigarrillo.isChecked
            val altura=et_Altura.text.toString().toInt()
            val colesterolTotal=et_Colesterol.text.toString().toInt()
            val peso=et_Peso.text.toString().toInt()
            val presionSistolica=et_presionSistolica.text.toString().toInt()
            val spo2=editTextSpO2.text.toString().toInt()
            val infoMedica= InfoMedicalModel(altura,colesterolTotal,consumoAlcohol,diabetico,fumador,peso,presionSistolica,spo2)
            saveUserInfo(infoMedica)
            val message=Utils().calcularRiesgo(infoMedica);
            showDefaultDialog(message)
        }
    }

    private fun toast(text: String) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    private fun showDefaultDialog(message:String) {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.apply {
            setIcon(R.drawable.heart_beat)
            setTitle("Nivel de riesgo")
            setMessage(message)
            setPositiveButton("Vamos a reducirlo") { _, _ ->
                toast("Revisa tus recomendaciones")
            }
            setNegativeButton("Meh") { _, _ ->
                toast("Prestar más cuidado a tu salud es necesario")
            }

        }.create().show()
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://us-central1-backendcorazonsaludable.cloudfunctions.net/medicalInfo/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun saveUserInfo(user: InfoMedicalModel){
        CoroutineScope(Dispatchers.IO).launch {
            val call = currentFirebaseUser?.let {
                while(!Utils().isConnected(this@Calculadora)){
                    continue
                }
                getRetrofit().create(InfoMedicaAPIService::class.java).updateUser(
                    it.uid,user)
            }
        }
    }
}