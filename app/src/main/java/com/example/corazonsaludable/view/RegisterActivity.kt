package com.example.corazonsaludable.view

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.corazonsaludable.R
import com.example.corazonsaludable.databinding.ActivityRegisterBinding
import com.example.corazonsaludable.tools.Utils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val intentIniciarSesion = Intent(this, LoginActivity::class.java)
        binding.textViewIniciaSesion.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                    startActivity(intentIniciarSesion)
            }
        })

        binding.buttonRegistro.setOnClickListener {
            val email = binding.editTextEmailSignUp.text.toString().trim { it <= ' ' }
            val email2 = binding.editTextVerificationEmailAddress.text.toString().trim { it <= ' ' }
            val password = binding.editTextPasswordSignUp.text.toString().trim { it <= ' ' }
            if (Utils().isConnected(this@RegisterActivity)) {
                when {
                    TextUtils.isEmpty(email) -> {
                        Toast.makeText(
                            this@RegisterActivity,
                            "Por favor ingrese un correo electrónico",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    TextUtils.isEmpty(password) -> {
                        Toast.makeText(
                            this@RegisterActivity,
                            "Por favor ingrese una contraseña",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    !(TextUtils.equals(email, email2)) -> {
                        Toast.makeText(
                            this@RegisterActivity,
                            "El correo no coincide",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    else -> {
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(
                                OnCompleteListener<AuthResult> { task ->
                                    if (task.isSuccessful) {
                                        val FirebaseUser: FirebaseUser = task.result!!.user!!

                                        Toast.makeText(
                                            this@RegisterActivity,
                                            "Registro exitoso!",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        val intent =
                                            Intent(this@RegisterActivity, user_info::class.java)
                                        intent.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                        finish()
                                    } else {
                                        Toast.makeText(
                                            this@RegisterActivity,
                                            task.exception!!.message.toString(),
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                })
                    }
                }

            }
            else{
                val builder = AlertDialog.Builder(this@RegisterActivity)
                val msg: String =
                    "Oops! Parece que no estás conectado a internet. Intentalo más tarde."
                val yes: String = "Entendido"
                builder.setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(yes) { dialog, _ ->
                        dialog.dismiss()
                    }
                val alert = builder.create()
                alert.show()
            }
        }
    }
}



