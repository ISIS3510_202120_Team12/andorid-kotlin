package com.example.corazonsaludable.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.corazonsaludable.R
import com.example.corazonsaludable.model.DataGimnasio
import com.example.corazonsaludable.model.DataRestaurante
import kotlinx.android.synthetic.main.item_list_recomendacion_dialog.view.*

class AdapterGimnasiosListDialog(
    private val context: Context,
    private val values: ArrayList<DataGimnasio>,
    private val listener: OnSelectCategorialistener
) : RecyclerView.Adapter<AdapterGimnasiosListDialog.ViewHolder>() {

    inner class ViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        var mClickCategory: LinearLayout = mView.item_recomendacion_click
        var mImgCategory: ImageView = mView.image_category_item
        var mName: TextView = mView.name_item_recomendacion
        var mDireccion: TextView = mView.direccion_item_recomendacion
        var mPrecio: TextView = mView.precio_item_recomendacion
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_list_recomendacion_dialog, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.mName.text = item.name
        holder.mDireccion.text = item.direccion
        holder.mPrecio.text = "$$$"
        holder.mClickCategory.setOnClickListener {
            listener.seleccionarGimnasio(item)
        }
    }

    interface OnSelectCategorialistener {
        fun seleccionarGimnasio(item: DataGimnasio)
    }
}