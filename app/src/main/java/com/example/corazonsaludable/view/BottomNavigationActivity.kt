package com.example.corazonsaludable.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.corazonsaludable.R
import com.example.corazonsaludable.tools.ManagerPreferences
import com.example.corazonsaludable.view.fragments.RecomendacionesFragment
import com.example.corazonsaludable.view.fragments.HomeFragment
import com.example.corazonsaludable.view.fragments.PerfilFragment
import kotlinx.android.synthetic.main.activity_bottom_navigation.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BottomNavigationActivity : AppCompatActivity() {

    private val beneficiosFragment = RecomendacionesFragment()
    private val homeFragment = HomeFragment()
    private val perfilFragment = PerfilFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)

        replaceFragment(homeFragment)
        //bottom_navigation.selectedItemId = R.id.FirstFragment

        // Se desactiva el reselect dell menu
        bottom_navigation.setOnItemReselectedListener {
        }

        bottom_navigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.FirstFragment -> replaceFragment(homeFragment)
                R.id.SecondFragment -> replaceFragment(beneficiosFragment)
                R.id.ThirdFragment -> replaceFragment(perfilFragment)
            }
            true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        // MultiThreading
        // Se ejecuta transaccion de fragment en otro hilo diferente al principal.
        if(!ManagerPreferences().getBoolean(this,"UIUpdate",false)){
            val intent = Intent(
                this@BottomNavigationActivity, user_info::class.java
            )
            startActivity(intent)
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.commit()
    }
}