package com.example.corazonsaludable.tools


object Constants {

    // Constantes para guardar las preferencias
    const val SESSION_PREFERENCES = "SESSION_PREFERENCES"
    const val CUSTOM_RESTAURANTES = "CUSTOM_RESTAURANTES"

    // CATEGORIAS
    const val CATEGORIA_NUTRIC = "Nutrición"
    const val CATEGORIA_EJERCIC = "Ejercicio"
    const val CATEGORIA_MEDITA = "Meditación"
    const val CATEGORIA_SUEN = "Sueño"

    // ITEMS DE CATEGORIA
    const val ITEM_CATEGORIA_RESTAURANTE = "Restaurantes"
    const val ITEM_CATEGORIA_GIMNASIO = "Gimnasios"

    // KEYS PARA PERSISTENCIA EN EL PERFIL
    const val USER_ID_FIREBASE = "USER_ID_FIREBASE"
}