package com.example.corazonsaludable.tools

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import com.example.corazonsaludable.model.InfoMedicalModel
import com.example.corazonsaludable.view.user_info

class Utils {

    /**
     * Se usa para abrir la aplicación de Waze dada
     * las coordenadas del lugar donde se quiere ir
     */
    fun launchWaze(context: Context, lat: Double, long: Double) {
        /**
         * Con esta url se pueder ir a Waze para que muestre el punto donde se quiere ir
         */
        val url = "https://waze.com/ul?ll=$lat,$long&z=16&navigate=yes"
        try {
            /**
             * Se abre la url de Waze, directamente en
             * el explorador del dispositvo
             */
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(intent)
        } catch (ex: ActivityNotFoundException) {
            // If Waze is not installed, open it in Google Play:
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"))
            context.startActivity(intent)
        }
    }

    /**
     * Method that indicates if there is any internet connection.
     */
    fun isConnected(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    fun calcularRiesgo(infoMedicalModel: InfoMedicalModel): String{
        var message="Riesgo "
        var total=0
        if(infoMedicalModel.consumoAlcohol){
            total+=1
        }
        if(infoMedicalModel.diabetico){
            total+=1
        }
        if(infoMedicalModel.fumador){
            total+=1
        }
        if(infoMedicalModel.colesterolTotal>160){
            total+=2
        }else if(infoMedicalModel.colesterolTotal>130){
            total+=1
        }
        if(infoMedicalModel.presionSistolica>160){
            total+=2
        }else if(infoMedicalModel.presionSistolica>130){
            total+=1
        }
        if(infoMedicalModel.spo2>160){
            total+=2
        }else if(infoMedicalModel.spo2>130){
            total+=1
        }
        if(infoMedicalModel.peso>160){
            total+=2
        }else if(infoMedicalModel.peso>130){
            total+=1
        }
        if(total>=10){
            message="Riesgo alto"
        }
        else if(total>=7){
            message="Riesgo medio"
        }
        return message
    }
}