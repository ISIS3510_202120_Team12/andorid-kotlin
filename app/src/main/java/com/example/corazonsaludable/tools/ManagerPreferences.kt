package com.example.corazonsaludable.tools

import android.content.Context
import com.example.corazonsaludable.tools.Constants.SESSION_PREFERENCES

class ManagerPreferences {

    fun putBoolean(context: Context, key: String, value: Boolean) {
        val editor = context
            .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE).edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getBoolean(context: Context, key: String, defValue: Boolean): Boolean {
        val shared = context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        return shared.getBoolean(key, defValue)
    }

    fun putInt(context: Context, key: String, value: Int) {
        val editor = context
            .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE).edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun getInt(context: Context, key: String, defValue: Int): Int {
        val shared = context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        return shared.getInt(key, defValue)
    }

    fun putFloat(context: Context, key: String, value: Float) {
        val editor = context
            .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE).edit()
        editor.putFloat(key, value)
        editor.apply()
    }

    fun getFloat(context: Context, key: String, defValue: Float): Float {
        val shared = context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        return shared.getFloat(key, defValue)
    }

    fun putLong(context: Context, key: String, value: Long) {
        val editor = context
            .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE).edit()
        editor.putLong(key, value)
        editor.apply()
    }

    fun getLong(context: Context, key: String, defValue: Long): Long {
        val shared = context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        return shared.getLong(key, defValue)
    }

    fun putString(context: Context, key: String, value: String) {
        val shared = context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        val editor = shared.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun getString(context: Context, key: String, defValue: String): String? {
        val shared =
            context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        return shared.getString(key, defValue)
    }

    fun clear(context: Context) {
        val shared = context.getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE)
        shared.edit().clear().apply()
    }
}