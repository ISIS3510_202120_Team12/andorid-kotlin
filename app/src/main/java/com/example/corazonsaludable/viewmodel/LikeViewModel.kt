package com.example.corazonsaludable.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.corazonsaludable.model.LikeModel

class LikeViewModel: ViewModel() {
    val likeModel = MutableLiveData<LikeModel>()
}