package com.example.corazonsaludable.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.corazonsaludable.model.DataGimnasio
import com.example.corazonsaludable.model.DataRestaurante
import com.example.corazonsaludable.model.PreferencesModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RecomendacionesViewModel : ViewModel() {
    val preferencesModel = MutableLiveData<PreferencesModel>()
    val db = Firebase.firestore

    /**
     * Funcion que retorna los restaurantes disponibles en la base de datos.
     * @return list lista de restaurantes
     *
     */
    fun getRestaurantes(listener: OnResponseRestaurantes) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val restaurantes: ArrayList<DataRestaurante> = ArrayList()
                db.collection("nutricion")
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            val dataObj = document.data
                            val gps = document.getGeoPoint("gps")
                            Log.e("GPS", gps!!.latitude.toString())
                            val current = DataRestaurante(
                                dataObj["nombre"].toString(),
                                dataObj["direccion"].toString(),
                                dataObj["precio"].toString(),
                                gps.latitude,
                                gps.longitude
                            )
                            restaurantes.add(current)
                            Log.d("DB_ACCESS", "${document.id} => ${document.data}")
                        }
                        listener.onResponseSuccessFullRestaurantes(restaurantes)
                    }
                    .addOnFailureListener { exception ->
                        listener.onResponseSuccessFullRestaurantes(restaurantes)
                        Log.w("FAILURE", "Error getting documents.", exception)
                    }
            }
        }

    }

    /**
     * Funcion que retorna los gimnasios disponibles en la base de datos.
     * @return list lista de restaurantes
     *
     */
    fun getGimnasios(listener: OnResponseGimnasios) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val gimnasios: ArrayList<DataGimnasio> = ArrayList()
                db.collection("ejercicio")
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            val dataObj = document.data
                            val gps = document.getGeoPoint("gps")
                            Log.e("GPS", gps!!.latitude.toString())
                            val current = DataGimnasio(
                                dataObj["nombre"].toString(),
                                dataObj["direccion"].toString(),
                                gps.latitude,
                                gps.longitude
                            )
                            gimnasios.add(current)
                            Log.d("DB_ACCESS", "${document.id} => ${document.data}")
                        }
                        listener.onResponseSuccessFullGimnasios(gimnasios)
                    }
                    .addOnFailureListener { exception ->
                        listener.onResponseSuccessFullGimnasios(gimnasios)
                        Log.w("FAILURE", "Error getting documents.", exception)
                    }
            }
        }

    }

    /**
     * interface para retornar los restaurantes
     */
    interface OnResponseRestaurantes {
        fun onResponseSuccessFullRestaurantes(restaurantes: ArrayList<DataRestaurante>)
    }

    /**
     * interface para retornar los gimnasios
     */
    interface OnResponseGimnasios {
        fun onResponseSuccessFullGimnasios(gimnasios: ArrayList<DataGimnasio>)
    }


}