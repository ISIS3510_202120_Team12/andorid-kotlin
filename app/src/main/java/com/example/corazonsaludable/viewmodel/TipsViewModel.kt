package com.example.corazonsaludable.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.corazonsaludable.model.TipsModel

class TipsViewModel: ViewModel() {
    val tipsModel = MutableLiveData<TipsModel>()

}