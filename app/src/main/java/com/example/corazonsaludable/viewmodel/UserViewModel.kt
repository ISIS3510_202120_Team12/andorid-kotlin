package com.example.corazonsaludable.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.corazonsaludable.model.UserAPIService
import com.example.corazonsaludable.model.UserModel
import com.example.corazonsaludable.tools.Constants
import com.example.corazonsaludable.tools.ManagerPreferences
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class UserViewModel : ViewModel() {
    var currentFirebaseUser = FirebaseAuth.getInstance().currentUser?.uid
    val userModel = MutableLiveData<UserModel>()
    val db = Firebase.firestore

    /**
     * Funcion que retorna los restaurantes disponibles en la base de datos.
     * @return list lista de restaurantes
     *
     */
    fun getUserInfo(context: Context, listener: UserViewModel.OnResponseDataUser) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val userId = ManagerPreferences().getString(context, Constants.USER_ID_FIREBASE, "NOT_FOUND")
                db.collection("users").document(userId!!).get().addOnSuccessListener { result ->

                    val body = result["body"]
                    // val nombre = body["d"].toString()
                    Log.e("USER_INFO", result["body"].toString())
                }
            }
        }
    }

    /**
     * interface para retornar los restaurantes
     */
    interface OnResponseDataUser {
        fun onResponseSuccessFullDataUser(user: UserModel)
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://us-central1-backendcorazonsaludable.cloudfunctions.net/user/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun saveUserInfo(user: UserModel){
        CoroutineScope(Dispatchers.IO).launch {
            val call = currentFirebaseUser?.let {
                getRetrofit().create(UserAPIService::class.java).updateUser(
                    it,user)
            }
            }
        }
    }
