package com.example.corazonsaludable.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.corazonsaludable.model.PreferencesModel

class PreferencesViewModel: ViewModel() {
    val preferencesModel = MutableLiveData<PreferencesModel>()

}