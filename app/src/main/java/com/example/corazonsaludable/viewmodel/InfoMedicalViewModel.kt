package com.example.corazonsaludable.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.corazonsaludable.model.InfoMedicalModel

class InfoMedicalViewModel: ViewModel() {
    val infoMedicalModel = MutableLiveData<InfoMedicalModel>()

}