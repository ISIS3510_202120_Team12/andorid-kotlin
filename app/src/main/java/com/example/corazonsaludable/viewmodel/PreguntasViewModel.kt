package com.example.corazonsaludable.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.corazonsaludable.model.DataPregunta
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PreguntasViewModel : ViewModel() {
    val db = Firebase.firestore

    /**
     * Funcion que retorna las  preguntas en base de datos.
     * @return list lista de preguntas
     *
     */
    fun getPreguntas(listener: OnResponsePreguntas) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val preguntas: ArrayList<DataPregunta> = ArrayList()
                db.collection("preguntas")
                    .get()
                    .addOnSuccessListener { result ->
                        for (document in result) {
                            val dataObj = document.data
                            val current = DataPregunta(
                                dataObj["especialidad"].toString(),
                                dataObj["pregunta"].toString(),
                                dataObj["tema"].toString()
                            )
                            preguntas.add(current)
                            Log.d("DB_ACCESS", "${document.id} => ${document.data}")
                        }
                        listener.onResponseSuccessFullPreguntas(preguntas)
                    }
                    .addOnFailureListener { exception ->
                        listener.onResponseSuccessFullPreguntas(preguntas)
                        Log.w("FAILURE", "Error getting documents.", exception)
                    }
            }
        }
    }


    /**
     * interface para retornar los restaurantes
     */
    interface OnResponsePreguntas {
        fun onResponseSuccessFullPreguntas(preguntas: ArrayList<DataPregunta>)
    }


}